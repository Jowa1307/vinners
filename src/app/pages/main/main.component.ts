import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  competitors: Competitor[];
  constructor() {
    this.competitors = [
      {"name": "Johan", "wine": "Rivers-Marie"},
      {"name": "Kalle", "wine": "Terrazas De Los Andes"},
      {"name": "Martin", "wine": "Marchesi Antinori"}, 
      {"name": "Helena", "wine": "Badia A Coltibuon"},  
      {"name": "Patrik", "wine": "Merryvale"},
      {"name": "Per", "wine": "Chateau Palmer"},
      {"name": "Henrik", "wine": "Chateau Lynch"},
      {"name": "Ramin", "wine": "Chateau Talbot"},
      {"name": "Karin", "wine": "Chateau Gruaud"},
      {"name": "Jennie", "wine": "Chateau Lafon"},
      {"name": "Kristina", "wine": "Chateau Montrose"},
      {"name": "Kicki", "wine": "Delas Freres"},
      {"name": "Thomas", "wine": "Rivers-Marie"}
    ];
  }

  ngOnInit() {
  }


  addCompetitor(name: string) {    
    this.competitors.push(new Competitor(name, ""));
  }

  deleteCompetitor(competitor: any) {
    let idx = this.competitors.indexOf(competitor);
    if(idx !== -1) {
      this.competitors.splice(idx, 1);
    }
  }

}


export class Competitor{
  constructor(public name, public wine) { 
  }
}